<?php

/**
 *
 * @author Alexander Caicedo
 */
class DbLoadFile extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Consulta los tipos de usuarios
     * @return type
     * @throws Exception
     */
    public function getTheTypesUsers() {
        $query = $this->db->select('id, descripcion')
                ->get('tipo_usuarios');
        $error = $this->db->error();
        if ($error['message']) {throw new Exception($error['message'] . ' class' . __CLASS__ . ' line' . __LINE__);}
        return $query->result_array();
    }
    
    /**
     * Carga los usuarios a la bases de datos
     * @param type $data
     * @return type
     * @throws Exception
     */
    public function insertUsersBatch($data) {
        $insert = $this->db->insert_batch('usuarios', $data);
        $error = $this->db->error();
        if ($error['message']) {throw new Exception($error['message'] . ' class' . __CLASS__ . ' line' . __LINE__);}
        return $insert;
    }

    /**
     * Consulta los usuarios por tipo
     * @param type $type_user
     * @return type
     * @throws Exception
     */
    public function getUsers($type_user) {
        $query = $this->db->select('nombre, apellido, correo')
                ->where('id_tipo', $type_user)
                ->get('usuarios');
        $error = $this->db->error();
        if ($error['message']) {throw new Exception($error['message'] . ' class' . __CLASS__ . ' line' . __LINE__);}
        return $query->result_array();
    }
}

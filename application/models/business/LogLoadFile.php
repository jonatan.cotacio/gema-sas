<?php

/**
 * @author Alexander Caicedo
 */
class LogLoadFile extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->model("persistence/DbLoadFile","dbLoadFile");
    }

    /**
     * Lee y guarda los datos del archivo enviado
     * @author Jonatan Alexander Caicedo Caicedo <jonatan.cotacio@gmail.com>
     * @date 25/11/2020
     * @access public
     * @return type
     */
    public function fileUploadLogic() {
        $file = $_FILES["file"];
        if (!$file['name']) {return array('status' => 400, 'message' => 'El archivos es obligatorio');}

        $dataTmp = array();
        $dataInsert = array();

        /* Lectura de archivo */
        $fp = fopen($file["tmp_name"], "r");
        while (!feof($fp)) {
            $line = explode(",", fgets($fp));
            array_push($dataTmp, $line);
        }
        fclose($fp);

        /*Consulta los tipos de usuarios*/
        $db_type =$this->dbLoadFile->getTheTypesUsers();
        if (!$db_type) {return array('status' => 400, 'message' => 'Los tipos de usuarios no estan parametrizados');}
        $type = array_column($db_type, 'id');
        
        /*Validación y guardado de datos*/
        foreach ($dataTmp as $key => $value) {
            $id_type = '';
            /*Validación de id de tipo de usuario*/
            if(isset($value[3])){$id_type = trim($value[3]);}
            if($id_type == '' || !is_numeric($id_type)){return array('status' => 400, 'message' => 'El archivo no cumple con el formato requerido.');}
            if(!in_array($id_type, $type)){return array('status' => 400, 'message' => "El id {$id_type} de la fila ". ($key + 1)." no existe.");}
            
            $data = array (
                'nombre' => trim($value[1]),
                'apellido' => trim($value[2]),
                'correo' => trim($value[0]),
                'id_tipo' => $id_type
            );
            array_push($dataInsert, $data);
        }
        
        /*Inserción de datos*/
        $insert = $this->dbLoadFile->insertUsersBatch($dataInsert);
        if(!$insert){return array('status' => 400, 'message' => 'No se ha insertado ningun dato.');}
        
        return array(
            'status' => 200,
            'message' => 'Se ha insertado correctamete los usuarios'
        );
    }
    
    /**
     * Carga la lista de usuarios
     * @author Jonatan Alexander Caicedo Caicedo <jonatan.cotacio@gmail.com>
     * @date 25/11/2020
     * @access public
     * @return type
     */
    public function getListUsersLogic() {
        $db_type = $this->dbLoadFile->getTheTypesUsers();
        $data = array();
        foreach ($db_type as $value) {
            $dataTmp = array(
                'type' => $value['descripcion'],
                'data' => $this->dbLoadFile->getUsers($value['id'])
            );
            array_push($data, $dataTmp);
        }
        
        return array(
            'status' => 200,
            'message' => 'Lista de usuarios',
            'data' => $data
        );
    }

}

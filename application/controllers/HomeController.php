<?php

/**
 * Description of HomeConttroller
 * @author Jonatan Alexander Caicedo Caicedo
 */
class HomeController extends CI_Controller {

    function __construct() {
        parent::__construct();
    }
    
    /**
     * Carga la vista de inicio
     * @author Jonatan Alexander Caicedo Caicedo <jonatan.cotacio@gmail.com>
     * @date 25/11/2020
     * @access public
     * @return view
     */
    public function index() {
        $this->load->view('template', array(
            'viewBody' => 'home/index'
        ));
    }

}

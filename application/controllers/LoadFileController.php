<?php

/**
 * @author Jonatan Alexander Caicedo Caicedo
 */
class LoadFileController extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("business/LogLoadFile", "logLoadFile");
    }

    /**
     * Procesamiento de la carga de archivos
     * @author Jonatan Alexander Caicedo Caicedo <jonatan.cotacio@gmail.com>
     * @date 25/11/2020
     * @access public
     * @return JSON data|message|status
     */
    public function loadFileProcess() {
        try {
            $response = $this->logLoadFile->fileUploadLogic();
            header('Content-type: application/json; charset=utf-8');
            echo json_encode($response);
        } catch (Exception $exc) {
            echo json_encode(array(
                'status' => 400,
                'message' => 'Ocurrio un error por favor comuniquese con el administrador',
                'data' => $e->getMessage()
            ));
        }
    }

    /**
     * Procesa la lista de usuarios a mostrar
     * @author Jonatan Alexander Caicedo Caicedo <jonatan.cotacio@gmail.com>
     * @date 25/11/2020
     * @access public
     * @return JSON data|message|status
     */
    public function getListUsersProcess() {
        try {
            $response = $this->logLoadFile->getListUsersLogic();
            header('Content-type: application/json; charset=utf-8');
            echo json_encode($response);
        } catch (Exception $exc) {
            echo json_encode(array(
                'status' => 400,
                'message' => 'Ocurrio un error por favor comuniquese con el administrador',
                'data' => $e->getMessage()
            ));
        }
    }

}

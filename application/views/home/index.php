<div class="container" id="loadFile">
    <div class="row">
        <div class="col-12">
            <h1 class="text-info">Carga de archivos</h1>
        </div>
    </div>

    <div class="row justify-content-center mt-3">
        <div class="col-10">
            <div class="card">
                <div class="card-header">
                    GEMA SAS
                </div>
                <form id="formData" v-on:submit="loadData($event)">
                    <div class="card-body">
                        <h5 class="card-title">Formulario de carga de información</h5>
                        <div class="input-group ">
                            <div class="custom-file">
                                <input type="file" name="file" class="custom-file-input" placeholder="Examinar" lang="es" accept=".txt" id="file">
                                <label class="custom-file-label" id="basic-addon2" for="file">Examinar</label>
                            </div>
                        </div>
                        <small class="form-text mb-3" v-bind:class="{'text-success': response.status, 'text-danger': !response.status}">{{response.message}}</small>
                        <button type="submit" class="btn btn-info">Enviar formulario</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <hr class="bg-primary"/>
        </div>
        <?php $this->load->view('home/list') ?>
    </div>

</div>
<script src="<?= base_url('resources/js/Controllers/LoadFile/loadFileController.js?c=' . VAR_CACHE) ?>" type="text/javascript"></script>
<div class="col-12" v-for="d1 in data">
    <h3 class="text-info">{{d1.type}}</h3>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Correo</th>
                <th>Nombre</th>
                <th>Apellido</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="d2 in d1.data">
                <td>{{d2.correo}}</td>
                <td>{{d2.nombre}}</td>
                <td>{{d2.apellido}}</td>
            </tr>
        </tbody>
    </table>
</div>
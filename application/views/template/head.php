<head>
    <!--CSS-->
    <link href="<?= base_url('resources/css/styles/style.css?c=' . VAR_CACHE) ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('resources/css/bootstrap/bootstrap.min.css') ?>" type="text/css" rel="stylesheet"/>
    
    <!--JS-->
    <script src="<?= base_url('resources/js/jquery-3.5.1.min.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('resources/js/bootstrap/bootstrap.min.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url("node_modules/vue/dist/vue.js"); ?>" type="text/javascript"></script>
    <script src="<?= base_url("node_modules/vue-resource/dist/vue-resource.min.js"); ?>" type="text/javascript"></script>
</head>
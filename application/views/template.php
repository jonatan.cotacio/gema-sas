<!DOCTYPE html>
<html>
    <?php $this->load->view('template/head'); ?>
    <body>
        <?php $this->load->view('template/header'); ?>
        <?php $this->load->view($viewBody); ?>
    </body>
</html>

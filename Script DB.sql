create database jerrejerre;
use jerrejerre;

create table tipo_usuarios (
	id int not null primary key auto_increment,
    descripcion varchar(50) not null,
    fec_crea datetime default current_timestamp,
    fec_actualiza datetime
);
insert into tipo_usuarios (descripcion) values ('Activo'),('Inactivos'),('En espera');

select * from tipo_usuarios;

create table usuarios (
	id int not null primary key auto_increment,
    nombre varchar(60),
    apellido varchar(60),
    correo varchar(60),
    id_tipo int not null,
    fec_crea datetime default current_timestamp,
    fec_actualiza datetime,
    foreign key (id_tipo) references tipo_usuarios (id)
);

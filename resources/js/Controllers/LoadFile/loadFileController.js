var load = new Vue({
    el: '#loadFile',
    data: {
        response: {
            message: '',
            display: false,
            status: false
        },
        data: {}
    },
    methods: {
        loadData: (e) => {
            e.preventDefault();
            var formData = new FormData(document.getElementById("formData"));
            load.response.display = false;
            $.ajax({
                url: BASE_URL + "LoadFileController/loadFileProcess",
                type: "POST",
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: formData,
                success: function (response) {
                    load.response.display = true;
                    load.response.message = response.message;
                    load.response.status = response.status == 200 ? true : false;

                    load.getUsers();
                },
                error: function () {
                    load.response.display = true;
                    load.response.message = 'Ocurrio un error';
                    load.response.status = false;
                }
            });
        },

        getUsers: () => {
            this.data = {};
            $.ajax({
                url: BASE_URL + "LoadFileController/getListUsersProcess",
                type: "POST",
                dataType: 'json',
                success: function (response) {
                    load.data = response.data;
                }
            });
        }
    },
    mounted: function () {
        this.getUsers();
    }
});